#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include "vtkEventQtSlotConnect.h"
#include "dataset.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    vtkSmartPointer<vtkRenderer> dagrenderer;
    vtkSmartPointer<vtkRenderer> dagrenderer2;
    vtkSmartPointer<vtkRenderWindow> renWin;
    vtkSmartPointer<vtkRenderWindowInteractor> renInt;
    vtkSmartPointer<vtkPolyData> mesh;
    vtkSmartPointer<vtkDataSet> omesh;
    vtkSmartPointer<vtkPolyData> original_mesh;
    vtkSmartPointer<vtkPolyDataMapper> meshMapper;
    vtkSmartPointer<vtkActor> meshActor;
    vtkSmartPointer<vtkPolyData> allCriticalPoints;

    dataset *data;

    std::string scalarArrayName;
    int         scalarArrayNum;

    double simUpto;
    vector< double > randomColors;

public:
    explicit MainWindow(QWidget *parent = 0);
    void readPolyData(vtkPolyData *poly);
    void tagExtrema(vtkPolyData *poly);
    void displayBoundary();
    void dump_info_on_close();
    void mapToStream2(map<int, set<int> > map, ofstream& o);

    bool invert;
    ~MainWindow();
private slots:
    void on_flatten_clicked();
    void on_colorBox_currentIndexChanged(int index);
    void on_generateScreenshot_clicked();
    void on_simplifyUpto_clicked();
    void on_smooth_clicked();
    void on_checkBox_Pairing_clicked();

protected:
    void dumpPersistenceStatistics();
    void dumpInformation();
    void dumpSegmentedData();
private:
    Ui::MainWindow *ui;
    std::string filename;
};

#endif // MAINWINDOW_H
