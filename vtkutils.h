#ifndef VTKUTILS_H
#define VTKUTILS_H
#include<set>
#include<vector>
#include"vtkinc.h"
vtkSmartPointer<vtkIdList> GetConnectedVertices(vtkSmartPointer<vtkPolyData> mesh, int id);


bool isQuad(vtkPolyData* poly, int p1,int p2, int p3, int p4);
bool quadInCyclicOrder(vtkPolyData *poly, int p1,int p2, int p3, int p4, std::vector<int> *order);
#endif // VTKUTILS_H
