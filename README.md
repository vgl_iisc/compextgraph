# README #

This code constructs the complete extremum graph for a scalar
field provided as a .vtk file ( structured points, polydata, unstructured grid)

### Compiling and Running ###

* todo
* ./ceg filename.vtk  simplification_threshold(optional, default=1) maximum/minimum graph[1/0] (optional, default=0) scalar_array_name/scalar_array_number (optional, default =0)
### Requires ###

* VTK 6.1
* QT 5 

### Contact ###

*  Vidya Narayanan vid8687@gmail.com 
