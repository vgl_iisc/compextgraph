#include "vtkutils.h"


vtkSmartPointer<vtkIdList> GetConnectedVertices(vtkSmartPointer<vtkPolyData> mesh, int id)
{
    vtkSmartPointer<vtkIdList> connectedVertices =
            vtkSmartPointer<vtkIdList>::New();

    //get all cells that vertex 'id' is a part of
    vtkSmartPointer<vtkIdList> cellIdList =
            vtkSmartPointer<vtkIdList>::New();
    mesh->GetPointCells(id, cellIdList);


    for(vtkIdType i = 0; i < cellIdList->GetNumberOfIds(); i++)
    {

        int cell_id = cellIdList->GetId(i);
        int cell_type = mesh->GetCellType(cell_id);
        if(cell_type != VTK_LINE)
            continue;

        vtkSmartPointer<vtkIdList> pointIdList =
                vtkSmartPointer<vtkIdList>::New();
        mesh->GetCellPoints(cellIdList->GetId(i), pointIdList);

        for(int i = 0; i < pointIdList->GetNumberOfIds(); i++)
        {
            if(pointIdList->GetId(i) != id )
            {

                connectedVertices->InsertUniqueId(pointIdList->GetId(i));
            }
        }

    }


    return connectedVertices;
}


bool isQuad(vtkPolyData* poly, int p1,int p2, int p3, int p4)
{
    int cnt = 0;
    cnt += poly->IsEdge(p1,p2) + poly->IsEdge(p2,p3) + poly->IsEdge(p4,p3) + poly->IsEdge(p1,p4) + poly->IsEdge(p1,p3) + poly->IsEdge(p2,p4);
    return cnt==4;
}
bool quadInCyclicOrder(vtkPolyData *poly, int p1,int p2, int p3, int p4, std::vector<int> *order)
{
    int cnt = 0;
    cnt += poly->IsEdge(p1,p2) + poly->IsEdge(p2,p3) + poly->IsEdge(p4,p3) + poly->IsEdge(p1,p4) + poly->IsEdge(p1,p3) + poly->IsEdge(p2,p4);
    std::set<int> vs;
    vs.insert(p2);
    vs.insert(p3);
    vs.insert(p4);

    bool isq = false;
    vtkSmartPointer<vtkIdList> cellIdList1 =
            vtkSmartPointer<vtkIdList>::New();
    vtkSmartPointer<vtkIdList> cellIdList2 =
            vtkSmartPointer<vtkIdList>::New();
    vtkSmartPointer<vtkIdList> cellIdList3 =
            vtkSmartPointer<vtkIdList>::New();
    vtkSmartPointer<vtkIdList> cellIdList4 =
            vtkSmartPointer<vtkIdList>::New();
    poly->GetPointCells(p1,cellIdList1);
    poly->GetPointCells(p2,cellIdList2);
    poly->GetPointCells(p3,cellIdList3);
    poly->GetPointCells(p4,cellIdList4);

    cellIdList1->IntersectWith(cellIdList2);
    cellIdList1->IntersectWith(cellIdList3);
    cellIdList1->IntersectWith(cellIdList4);
    isq = cellIdList1->GetNumberOfIds() > 0 ? true: false;



    if(cnt == 4 && isq)
    {
        order->push_back(p1);
        int lastPushed = p1;
        while(!vs.empty())
        {
            for(std::set<int>::iterator it = vs.begin(); it!= vs.end(); it++)
            {
                int v = *it;
                if(poly->IsEdge(v,lastPushed))
                {
                    lastPushed = v;
                    order->push_back(v);
                    vs.erase(v);
                    break;
                }
            }
        }

        assert( order->size() == 4);

    }
    return (cnt==4 && isq);
}

