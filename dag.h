#ifndef DAG_H
#define DAG_H
#include <vector>
#include <set>
#include "curve.h"
using namespace std;
struct dagnode
{
    int node_id;
    point<double> position;
    std::set<int> next_nodes;
    std::set<int> prev_nodes;
};

class dag
{
public:
    std::vector<dagnode> nodes;
    double edge_prob;
    int num_nodes;

    dag();
    dag(int n, double p){num_nodes = n; edge_prob  = p;}
    void generate();
    bool **dg;
    ~dag();
};

#endif // DAG_H
