#include "dag.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

dag::dag()
{
}

void dag::generate()
{
    srand (time (NULL));
      dg = (bool**)malloc(sizeof(bool*)*num_nodes);
      for(int i =0 ;i < num_nodes; i++)
      {
          dg[i] = (bool*)malloc(sizeof(bool)*num_nodes);
          for(int j = 0; j < num_nodes; j++)
        {
            dg[i][j] = false;
        }
      }

      FILE * pFile;
      pFile = fopen ("temp.gv","w");


      fprintf (pFile,"digraph {\n");
        {
        for(int i = 0; i < num_nodes; i++)
        {
            for(int j = i+1; j < num_nodes; j++)
            {
                double chance = rand()/(double)RAND_MAX;
                if(chance < edge_prob)
                {
                    dg[i][j] = true;
                    fprintf(pFile, "%d -> %d; \n",i,j);
                }
            }
        }
        // add source and sink
        //printf("src -> 0; \n");
        for(int  i = 0; i < num_nodes; i++)
        {
            bool colzero = true;
            bool rowzero = true;
            for(int j = 0; j  < i+1; j++)
            {
                if(dg[j][i])
                {
                    colzero = false;
                    break;
                }
            }
            if(colzero)
            {
                fprintf(pFile," src -> %d ;\n", i );
            }
            for(int j = i; j < num_nodes; j++)
            {
                if(dg[i][j])
                {
                    rowzero = false;
                    break;
                }
            }
            if(rowzero)
            {
                fprintf(pFile, " %d -> sink ;\n", i);
            }
        }
        }


      fprintf (pFile,"}\n");

}
dag::~dag()
{
    for(int i = 0; i < num_nodes; i++)
    {
      free(dg[i]);
    }
    free(dg);
}
