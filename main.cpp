#include <QtWidgets/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    int retval = 0;
    w.show();
    try{
        retval = a.exec();
    }
    catch(...)
    {
        throw;
    }

    return retval;
}
