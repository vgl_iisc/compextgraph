#ifndef CURVE_H
#define CURVE_H
#include <vector>
#include "vtkProcrustesAlignmentFilter.h"
#include "vtkMultiBlockDataGroupFilter.h"

using namespace std;
enum COMPARE_OPT{Cos,Fretchet,Procrustes,Lsqr,Curvature, ICP};
template < class data_type >
class point
{
private:
    data_type x; data_type y; data_type z;
public:
    point(){}

    void set(data_type i, data_type j, data_type k)
    {
        x = i; y = j; z = k;
    }
    data_type* get3()
    {
        data_type *p = new double[3];
        p[0] = x; p[1] = y; p[2] = z;
        return p;
    }
    data_type getx(){return x;}
    data_type gety(){return y;}
    data_type getz(){return z;}
    data_type dist(point<data_type> p)
    {
        data_type xval = this->x - p.getx();
        data_type yval = this->y - p.gety();
        data_type zval = this->z - p.getz();
        return sqrt(xval*xval + yval*yval + zval*zval);
    }
    data_type len()
    {
        return sqrt(this->x*this->x + this->y*this->y + this->z*this->z);
    }
    void normalize()
    {
        data_type length = len();
        if(length > 0)
        {
            this->x = this->x / length;
            this->y = this->y / length;
            this->z = this->z / length;
        }
    }

};


class curve
{
public:
    vector< point<double> > pts;

public:
    curve();
    double compare(curve *c, COMPARE_OPT = Cos );
    void setPoints(vector< point<double> > polyPts);
    int getNumPts(){return pts.size(); }
};

#endif // CURVE_H
