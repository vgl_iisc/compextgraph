/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QVTKWidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *Curve;
    QHBoxLayout *horizontalLayout_5;
    QVTKWidget *qvtkWidget;
    QVTKWidget *qvtkWidget_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pushButton;
    QPushButton *normalizeButton;
    QPushButton *scaleButton;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *incSamples_button;
    QSpinBox *sampleSpinBox;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *computeOptions;
    QPushButton *computeButton;
    QHBoxLayout *horizontalLayout;
    QLabel *ScoreLabel;
    QLabel *ScoreText;
    QWidget *Dag;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout_7;
    QVTKWidget *qvtkWidget_4;
    QVTKWidget *qvtkWidget_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *dag_compute;
    QPushButton *generateScreenshot;
    QPushButton *simplifyUpto;
    QDoubleSpinBox *doubleSpinBox;
    QPushButton *smooth;
    QCheckBox *flatten;
    QComboBox *colorBox;
    QCheckBox *checkBox_Pairing;
    QTextBrowser *textBrowser;
    QWidget *tab;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(878, 851);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        Curve = new QWidget();
        Curve->setObjectName(QStringLiteral("Curve"));
        Curve->setEnabled(false);
        horizontalLayout_5 = new QHBoxLayout(Curve);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        qvtkWidget = new QVTKWidget(Curve);
        qvtkWidget->setObjectName(QStringLiteral("qvtkWidget"));

        horizontalLayout_5->addWidget(qvtkWidget);

        qvtkWidget_2 = new QVTKWidget(Curve);
        qvtkWidget_2->setObjectName(QStringLiteral("qvtkWidget_2"));

        horizontalLayout_5->addWidget(qvtkWidget_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        pushButton = new QPushButton(Curve);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_4->addWidget(pushButton);

        normalizeButton = new QPushButton(Curve);
        normalizeButton->setObjectName(QStringLiteral("normalizeButton"));

        horizontalLayout_4->addWidget(normalizeButton);

        scaleButton = new QPushButton(Curve);
        scaleButton->setObjectName(QStringLiteral("scaleButton"));

        horizontalLayout_4->addWidget(scaleButton);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        incSamples_button = new QPushButton(Curve);
        incSamples_button->setObjectName(QStringLiteral("incSamples_button"));

        horizontalLayout_2->addWidget(incSamples_button);

        sampleSpinBox = new QSpinBox(Curve);
        sampleSpinBox->setObjectName(QStringLiteral("sampleSpinBox"));
        sampleSpinBox->setMinimum(2);
        sampleSpinBox->setMaximum(500);
        sampleSpinBox->setValue(30);

        horizontalLayout_2->addWidget(sampleSpinBox);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        computeOptions = new QComboBox(Curve);
        computeOptions->setObjectName(QStringLiteral("computeOptions"));

        horizontalLayout_3->addWidget(computeOptions);

        computeButton = new QPushButton(Curve);
        computeButton->setObjectName(QStringLiteral("computeButton"));

        horizontalLayout_3->addWidget(computeButton);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        ScoreLabel = new QLabel(Curve);
        ScoreLabel->setObjectName(QStringLiteral("ScoreLabel"));
        ScoreLabel->setLayoutDirection(Qt::LeftToRight);

        horizontalLayout->addWidget(ScoreLabel);

        ScoreText = new QLabel(Curve);
        ScoreText->setObjectName(QStringLiteral("ScoreText"));

        horizontalLayout->addWidget(ScoreText);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_5->addLayout(verticalLayout);

        tabWidget->addTab(Curve, QString());
        qvtkWidget->raise();
        qvtkWidget_2->raise();
        Dag = new QWidget();
        Dag->setObjectName(QStringLiteral("Dag"));
        gridLayout_2 = new QGridLayout(Dag);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        qvtkWidget_4 = new QVTKWidget(Dag);
        qvtkWidget_4->setObjectName(QStringLiteral("qvtkWidget_4"));

        horizontalLayout_7->addWidget(qvtkWidget_4);

        qvtkWidget_3 = new QVTKWidget(Dag);
        qvtkWidget_3->setObjectName(QStringLiteral("qvtkWidget_3"));

        horizontalLayout_7->addWidget(qvtkWidget_3);


        horizontalLayout_6->addLayout(horizontalLayout_7);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        dag_compute = new QPushButton(Dag);
        dag_compute->setObjectName(QStringLiteral("dag_compute"));

        horizontalLayout_8->addWidget(dag_compute);


        verticalLayout_2->addLayout(horizontalLayout_8);

        generateScreenshot = new QPushButton(Dag);
        generateScreenshot->setObjectName(QStringLiteral("generateScreenshot"));

        verticalLayout_2->addWidget(generateScreenshot);

        simplifyUpto = new QPushButton(Dag);
        simplifyUpto->setObjectName(QStringLiteral("simplifyUpto"));
        simplifyUpto->setEnabled(false);

        verticalLayout_2->addWidget(simplifyUpto);

        doubleSpinBox = new QDoubleSpinBox(Dag);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setDecimals(6);
        doubleSpinBox->setMaximum(2);
        doubleSpinBox->setSingleStep(0.01);
        doubleSpinBox->setValue(0.001);

        verticalLayout_2->addWidget(doubleSpinBox);

        smooth = new QPushButton(Dag);
        smooth->setObjectName(QStringLiteral("smooth"));
        smooth->setEnabled(false);

        verticalLayout_2->addWidget(smooth);

        flatten = new QCheckBox(Dag);
        flatten->setObjectName(QStringLiteral("flatten"));
        flatten->setEnabled(false);
        flatten->setChecked(true);

        verticalLayout_2->addWidget(flatten);

        colorBox = new QComboBox(Dag);
        colorBox->setObjectName(QStringLiteral("colorBox"));
        colorBox->setEnabled(false);

        verticalLayout_2->addWidget(colorBox);

        checkBox_Pairing = new QCheckBox(Dag);
        checkBox_Pairing->setObjectName(QStringLiteral("checkBox_Pairing"));
        checkBox_Pairing->setEnabled(false);
        checkBox_Pairing->setChecked(false);

        verticalLayout_2->addWidget(checkBox_Pairing);


        horizontalLayout_6->addLayout(verticalLayout_2);


        verticalLayout_3->addLayout(horizontalLayout_6);

        textBrowser = new QTextBrowser(Dag);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        verticalLayout_3->addWidget(textBrowser);


        gridLayout_2->addLayout(verticalLayout_3, 0, 0, 1, 1);

        tabWidget->addTab(Dag, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        tab->setEnabled(false);
        tabWidget->addTab(tab, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 878, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Add Lines", 0));
        normalizeButton->setText(QApplication::translate("MainWindow", "Normalize Lines", 0));
        scaleButton->setText(QApplication::translate("MainWindow", "Scale Lines", 0));
        incSamples_button->setText(QApplication::translate("MainWindow", "//Increase Samples", 0));
        computeButton->setText(QApplication::translate("MainWindow", "Compute Similarity", 0));
        ScoreLabel->setText(QApplication::translate("MainWindow", "  Score", 0));
        ScoreText->setText(QApplication::translate("MainWindow", "0", 0));
        tabWidget->setTabText(tabWidget->indexOf(Curve), QApplication::translate("MainWindow", "Curve Similarity", 0));
        dag_compute->setText(QApplication::translate("MainWindow", "Compute ", 0));
        generateScreenshot->setText(QApplication::translate("MainWindow", "Screenshot", 0));
        simplifyUpto->setText(QApplication::translate("MainWindow", "Simplify", 0));
        smooth->setText(QApplication::translate("MainWindow", "Smooth", 0));
        flatten->setText(QApplication::translate("MainWindow", "Flatten(/Smoothen)", 0));
        colorBox->clear();
        colorBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Function", 0)
         << QApplication::translate("MainWindow", "MS Cells", 0)
         << QApplication::translate("MainWindow", "Maxima", 0)
         << QApplication::translate("MainWindow", "Minima", 0)
         << QApplication::translate("MainWindow", "Max Scores", 0)
         << QApplication::translate("MainWindow", "Gradients", 0)
        );
        checkBox_Pairing->setText(QApplication::translate("MainWindow", "Show Pairing", 0));
        tabWidget->setTabText(tabWidget->indexOf(Dag), QApplication::translate("MainWindow", "Dag Similarity", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Region Similarity", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
